# ethereum-in-memory-simple-server

> Some tests using `web3` and `ethereum in-memory blockchain` for Node.js

## Dependencies

* [Ganache-core](https://github.com/trufflesuite/ganache-core) for local development blockchain
* [Web3](https://github.com/ethereum/web3.js/) as Ethereum JavaScript API

## Getting started

```
$ yarn install
$ yarn test
$ yarn start
```

## Default Configuration

```js
module.exports = {
	development: {
		protocol: "http",
		host: "localhost",
		port: 8545,
		network_id: 5777,
		total_accounts: 10,
		unlocked_accounts: [],
		mnemonic:
			"candy maple cake sugar pudding cream honey rich smooth crumble sweet treat",
	},
};
```

## TODO

* See what is going on out there

## License

MIT