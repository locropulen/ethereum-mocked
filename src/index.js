"use strict";

// const ethereum = require("./");
// const Web3 = require("web3");
const ganache = require("ganache-core");

const { host, port, network_id } = require("../config").development;

const options = {
	port,
	network_id,
	logger: {
		log: message => {
			if (message && message.indexOf(" ") == 0) {
				console.log(message);
			}
		},
	},
};

const server = ganache.server(options);

// let blockInterval;
// let lastBlock;

server.listen(port, host, function(err, blockchain) {
	if (err) {
		return;
	}
	console.log("Ganache started successfully!");
	console.log(blockchain);

	// const provider = new Web3.providers.HttpProvider(
	// 	"http://" + options.hostname + ":" + options.port
	// );

	// const web3 = ethereum.web3({}, provider);

	// console.log(web3.version);
	// blockInterval = setInterval(function() {
	// 	console.log(`web3.eth.blockNumber=${web3.eth.blockNumber}`);
	// }, 1000);
});

process.on("uncaughtException", err => {
	console.log(err.stack || err);
	process.send({ type: "error", data: err.stack || err });
});
