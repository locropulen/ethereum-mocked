"use strict";

module.exports = {
	development: {
		protocol: "http",
		host: "localhost",
		port: 8545,
		network_id: 5777,
		total_accounts: 10,
		unlocked_accounts: [],
		mnemonic:
			"candy maple cake sugar pudding cream honey rich smooth crumble sweet treat",
	},
};
