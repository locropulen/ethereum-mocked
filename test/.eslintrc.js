module.exports = {
	extends: "../.eslintrc.js",
	globals: {
		describe: true,
		beforeEach: true,
		it: true,
	},
};
