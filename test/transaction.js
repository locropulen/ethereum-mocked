"use strict";

const assert = require("assert");
const ethereum = require("./");

let web3;
let newAccount;

// https://web3js.readthedocs.io/en/1.0/web3-eth-accounts.html#create
describe("transaction", function() {
	beforeEach(function() {
		const options = {
			total_accounts: 1,
		};
		web3 = ethereum.web3(options);
		newAccount = web3.eth.accounts.create();
		assert.ok(newAccount);
	});
	it("should create a transaction", async function() {
		const accounts = await web3.eth.getAccounts();
		const balance = await web3.eth.getBalance(accounts[0]);

		if (balance < 100) {
			throw new Error("Default Balance is 1000000");
		}
		// https://web3js.readthedocs.io/en/1.0/web3-eth.html#id62
		// The callback will return the 32 bytes transaction hash.
		const tx = await web3.eth.sendTransaction({
			from: accounts[0],
			to: newAccount.address,
			value: 2, // 2 ethereums
		});
		assert.ok(tx.transactionHash);
		// assert.ok(tx.transactionIndex);
		assert.ok(tx.blockHash);
		assert.ok(tx.blockNumber);
		assert.ok(tx.gasUsed);
		assert.equal(tx.status, 1);
	});
	it("should create a signed transaction (unimplemented)", function() {
		// https://web3js.readthedocs.io/en/1.0/web3-eth.html#signtransaction
		// web3.eth.signTransaction({
		// 	from: "0xEB014f8c8B418Db6b45774c326A0E64C78914dC0",
		// 	gasPrice: "20000000000",
		// 	gas: "21000",
		// 	to: '0x3535353535353535353535353535353535353535',
		// 	value: "1000000000000000000",
		// 	data: ""
		// }).then(console.log);
		// > {
		// 	raw: '0xf86c808504a817c800825208943535353535353535353535353535353535353535880de0b6b3a76400008025a04f4c17305743700648bc4f6cd3038ec6f6af0df73e31757007b7f59df7bee88da07e1941b264348e80c78c4027afc65a87b0a5e43e86742b8ca0823584c6788fd0',
		// 	tx: {
		// 		nonce: '0x0',
		// 		gasPrice: '0x4a817c800',
		// 		gas: '0x5208',
		// 		to: '0x3535353535353535353535353535353535353535',
		// 		value: '0xde0b6b3a7640000',
		// 		input: '0x',
		// 		v: '0x25',
		// 		r: '0x4f4c17305743700648bc4f6cd3038ec6f6af0df73e31757007b7f59df7bee88d',
		// 		s: '0x7e1941b264348e80c78c4027afc65a87b0a5e43e86742b8ca0823584c6788fd0',
		// 		hash: '0xda3be87732110de6c1354c83770aae630ede9ac308d9f7b399ecfba23d923384'
		// 	}
		// }
	});
});
