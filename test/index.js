"use strict";

const Web3 = require("web3");
const ganache = require("ganache-core");
const { protocol, host, port } = require("../config").development;

module.exports = {
	// available options
	// "accounts": Array of Object's. Each object should have a balance key with a hexadecimal value. The key secretKey can also be specified, which represents the account's private key. If no secretKey, the address is auto-generated with the given balance. If specified, the key is used to determine the account's address.
	// "debug": boolean - Output VM opcodes for debugging
	// "logger": Object - Object, like console, that implements a log() function.
	// "mnemonic": Use a specific HD wallet mnemonic to generate initial addresses.
	// "port": Port number to listen on when running as a server.
	// "seed": Use arbitrary data to generate the HD wallet mnemonic to be used.
	// "total_accounts": number - Number of accounts to generate at startup.
	// "fork": string - Same as --fork option above.
	// "network_id": integer - Same as --networkId option above.
	// "time": Date - Date that the first block should start. Use this feature, along with the evm_increaseTime method to test time-dependent code.
	// "locked": boolean - whether or not accounts are locked by default.
	// "unlocked_accounts": Array - array of addresses or address indexes specifying which accounts should be unlocked.
	// "db_path": String - Specify a path to a directory to save the chain database. If a database already exists, ganache-cli will initialize that chain instead of creating a new one.
	// "account_keys_path": String - Specifies a file to save accounts and private keys to, for testing.
	web3: (options, provider) => {
		let web3;
		if (!provider) {
			web3 = new Web3(
				new Web3.providers.HttpProvider(`${protocol}://${host}:${port}`)
			);
		} else {
			web3 = new Web3();	
		}
		web3.setProvider(ganache.provider(options));
		return web3;
	},
};
