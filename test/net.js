"use strict";

const assert = require("assert");
const ethereum = require("./");

let web3;
describe("net", function() {
	beforeEach(function() {
		const options = {};
		web3 = ethereum.web3(options);
	});
	it("should listen", async function() {
		const isListening = await web3.eth.net.isListening();
		assert.equal(isListening, true);
	});
	it("should instance web3 server", function() {
		assert.equal(web3.version, "1.0.0-beta.27");
	});
	// it("should peer connected", async function() {
	// 	const count = await web3.eth.net.getPeerCount();
	// 	assert.equal(count, 1);
	// });
});
