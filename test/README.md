# ethereum-mocked

## Example sending a transaction ( super basic )

```js
"use strict";

const assert = require("assert");
const ethereum = require("../src/index");

let web3;
let account;
const options = {
	total_accounts: 1,
};
describe("account", function() {
	beforeEach(function() {
		web3 = ethereum.web3("http://localhost:8545", options);
		// address - string: The account address.
		// privateKey - string: The accounts private key. This should never be shared or stored unencrypted in localstorage! Also make sure to null the memory after usage.
		// signTransaction(tx [, callback]) - Function: The function to sign transactions. See web3.eth.accounts.signTransaction() for more.
		// sign(data) - Function: The function to sign transactions. See web3.eth.accounts.sign() for more.
		account = web3.eth.accounts.create();
		assert.ok(account);
	});
	it("should create a transaction", async function() {
		const accounts = await web3.eth.getAccounts();
		const balance = await web3.eth.getBalance(accounts[0]);

		if (balance < 100) {
			throw new Error("Ganache makes default Balance to 1000000 :O");
		}
		// https://web3js.readthedocs.io/en/1.0/web3-eth.html#id62
		// The callback will return the 32 bytes transaction hash.
		const tx = await web3.eth.sendTransaction({
			from: accounts[0],
			to: newAccount.address,
			value: 2, // 2 ethereums
		});
		assert.ok(tx.transactionHash);
		// assert.ok(tx.transactionIndex);
		assert.ok(tx.blockHash);
		assert.ok(tx.blockNumber);
		assert.ok(tx.gasUsed);
		assert.equal(tx.status, 1);
	});
});
```
