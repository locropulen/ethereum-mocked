"use strict";

const assert = require("assert");
const ethereum = require("./");

let web3;
let account;

// https://web3js.readthedocs.io/en/1.0/web3-eth-accounts.html#create
describe("account", function() {
	beforeEach(function() {
		const options = {
			total_accounts: 1,
		};
		web3 = ethereum.web3(options);
		// address - string: The account address.
		// privateKey - string: The accounts private key. This should never be shared or stored unencrypted in localstorage! Also make sure to null the memory after usage.
		// signTransaction(tx [, callback]) - Function: The function to sign transactions. See web3.eth.accounts.signTransaction() for more.
		// sign(data) - Function: The function to sign transactions. See web3.eth.accounts.sign() for more.
		account = web3.eth.accounts.create();
		assert.ok(account);
	});
	it("should create an account", async function() {
		const accounts = await web3.eth.getAccounts();
		assert.equal(accounts.length, 1, "already existing accoutn");
		assert.ok(account.address, "new account has address");
		assert.ok(account.privateKey, "new accoutn hask ey");
	});
});
