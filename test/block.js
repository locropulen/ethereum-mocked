"use strict";

// https://github.com/ethereum/wiki/wiki/JavaScript-API#web3ethdefaultblock
const assert = require("assert");
const ethereum = require("./");

let web3;

// https://github.com/ethereum/web3.js/pull/564/commits/550ffd6f225075b394ec89fe389b0c6265b7819a
describe("block", function() {
	beforeEach(function() {
		const options = {
			total_accounts: 10,
		};
		web3 = ethereum.web3(options);
	});
	it("should default block", async function() {
		assert.equal(web3.eth.defaultBlock, "latest");
	});
	it("should get accounts", async function() {
		const accounts = await web3.eth.getAccounts();
		assert.equal(accounts.length, 10);
	});
	// console.log(web3.eth.blockNumber);
});
